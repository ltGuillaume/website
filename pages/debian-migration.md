---
title: Debian Migration Guide
---

![](/icons/debian.svg)

# Debian Migration Guide

The structure of our Debian repository has changed. `https://deb.librewolf.net`
has been replaced by `https://repo.librewolf.net`. If you previously used the
old repository, please run the following commands in a terminal to move to the
new repository and continue to receive updates.

**Only follow these steps if you installed LibreWolf through APT on a
Debian-like distro.** If you installed LibreWolf using Flatpak (often just
called something like "Software Center"), or by other means, you do **not** need
to follow these steps, as that could result in errors or you having LibreWolf
installed two times.

### 1. Remove the old repository from your PC

```bash
sudo rm -f \
  /etc/apt/sources.list.d/librewolf.sources \
  /etc/apt/keyrings/librewolf.gpg \
  /etc/apt/preferences.d/librewolf.pref \
  /etc/apt/sources.list.d/librewolf.list \
  /etc/apt/trusted.gpg.d/librewolf.gpg
```

### 2. Add the new repository to your PC

```bash
sudo apt update && sudo apt install extrepo -y

sudo extrepo enable librewolf

sudo apt --allow-releaseinfo-change update
```

### 3. Update LibreWolf (optional)

```bash
sudo apt upgrade librewolf -y
```

## FAQ

### Why is this necessary?

Our build process has fundamentally changed. We are now offering a single
LibreWolf .deb for all Debian-based distributions instead of building for each
distro separately. Our old repository could not account for that change. In
addition, we used this opportunity to switch to a different GPG key.

### What is extrepo?

[Extrepo](https://manpages.debian.org/unstable/extrepo/extrepo.1p.en.html) is an
official Debian tool to more easily manage external repositories. It does not
ship with every Debian distribution by default, so we also include instructions
to install extrepo itself.

### Can I continue using the old repository?

No, it will not be provided with updated versions anymore and should already
contain an expired signature right now.

### How do I know whether I am still using the old repository?

The new repository will be registered at
`/etc/apt/sources.list.d/extrepo_librewolf.sources`, so if that is a valid
existing file on your system, you are already using the new repository. To check
that, you can run: 
```
ls /etc/apt/sources.list.d/extrepo_librewolf.sources
```
If this does not result in an error like 
`ls: cannot access '/etc/apt/sources.list.d/extrepo_librewolf.sources': No such file or directory`, 
you are already using the new repository. If you are using the old repository,
you will probably also see an error like this when trying to run `sudo apt update`:

```
Get:4 https://deb.librewolf.net bookworm InRelease [16.7 kB]
Err:4 https://deb.librewolf.net bookworm InRelease
 The following signatures were invalid: EXPKEYSIG D29FBD5F93C0CFC3 Malte Jürgens <maltejur@dismail.de>
Reading package lists... Done
W: GPG error: https://deb.librewolf.net bookworm InRelease: The following signatures were invalid: EXPKEYSIG D29FBD5F93C0CFC3 Malte Jürgens <maltejur@dismail.de>
E: The repository 'https://deb.librewolf.net bookworm InRelease' is not signed.
N: Updating from such a repository can't be done securely, and is therefore disabled by default.
N: See apt-secure(8) manpage for repository creation and user configuration details. 
```
