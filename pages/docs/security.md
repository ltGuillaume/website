# Report a Security Issue

If you want to report a security issue, please send it to
[security-issues@librewolf.net](mailto:security-issues@librewolf.net). Please
send that email PGP encrypted using
[this key](https://keys.openpgp.org/vks/v1/by-fingerprint/892940311B95BCF8A6B25EED9CB760109F0C8D93)
and double check that the fingerprint is
`8929 4031 1B95 BCF8 A6B2 5EED 9CB7 6010 9F0C 8D93`.

All emails sent to this address are forwarded to the core maintainers
threadpanic, maltejur and ohfp.

Please only use this email address for security issues that should be hidden from the public bugtracker. Please also note that we don't have any sort of "bug bounties".
